Package: metaBmisc
Type: Package
Title: Miscellaneous R functions for metabarcoding analysis
Version: 0.1.0
Authors@R: person("Cyril", "Noël", email = "cyril.noel@ifremer.fr", role = c("aut", "cre"), comment = c(ORCID = "https://orcid.org/0000-0002-7139-4073"))
Description: metaBmisc package contains miscellaneous functions for metabarcoding mainly based on phyloseq R package, including data manipulations, taxonomy handlers (Silva, Greengenes, PR2, ...), phyloseq-shortcuts (data normalisation, phyloseq splitting and exporting) and graphic visualization.
Depends: 
    R (>= 4.3.1),
    BiocGenerics (>= 0.46.0),
    ggplot2 (>= 3.5.0),
    microViz (>= 0.12.3),
    phyloseq (>= 1.44.0),
    reshape2 (>= 1.4.4),
    rstatix (>= 0.7.2),
    stringr (>= 1.5.1),
    vegan (>= 2.6.4)
Imports:
    classInt (>= 0.4.10),
    ComplexUpset (>= 1.3.3),
    decontam (>= 1.20.0),
    DESeq2 (>= 1.40.2),
    dplyr (>= 1.1.4),
    ggh4x (>= 0.2.8),
    ggmsa (>= 1.3.4),
    ggnested (>= 0.1.0),
    ggpubr (>= 0.6.0),
    ggsci (>= 3.1.0),
    gplots (>= 3.1.3.1),
    iNEXT (>= 3.0.1),
    lemon (>= 0.4.9),
    magrittr (>= 2.0.3),
    metagMisc (>= 0.5.0),
    microbiome (>= 1.22.0),
    microbiomeMarker (>= 1.6.0),
    muscle (>= 3.42.0),
    paletteer (>= 1.6.0),
    randomcoloR (>= 1.1.0.1),
    RColorBrewer (>= 1.1.3),
    SpiecEasi (>= 1.1.3),
    svMisc (>= 1.2.3),
    tibble (>= 3.2.1),
    tidyr (>= 1.3.1),
    WGCNA (>= 1.72.5)
Suggests:
Remotes: 
    vmikk/metagMisc,
    zdk123/SpiecEasi,
    stefpeschel/NetCoMi,
    abhik1368/netpredictor
License: MIT
Encoding: UTF-8
LazyData: true
RoxygenNote: 7.3.1
biocViews: Metagenomics, Microbiome, GraphAndNetwork
