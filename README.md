# ![metaBmisc](./assets/metaBmisc-logo.png)

<!-- badges: start -->

[![Latest release](https://img.shields.io/badge/Latest%20release-alpha-red?labelColor=000000)]()
[![Release date](https://img.shields.io/badge/Release%20date-no%20release%20currently-red?labelColor=000000)]()
[![Author](https://img.shields.io/badge/Author-Cyril%20Noël-yellow?labelColor=000000)](https://github.com/cnoel-sebimer)
[![R](https://img.shields.io/badge/R%20version-%3E%3D4.3.1-blue?labelColor=000000)](https://github.com/cnoel-sebimer)
[![DOI](https://img.shields.io/badge/DOI-not%20published-grey?labelColor=000000)]()
[![CRAN_Status_Badge](https://img.shields.io/badge/CRAN-not%20published-grey?labelColor=000000)](https://cran.r-project.org/package=metaBmisc)

<!-- badges: end -->

# Miscellaneous R functions for metabarcoding analysis  

metaBmisc package contains miscellaneous functions for metabarcoding mainly based on [*metagMisc*](https://github.com/vmikk/metagMisc) and [*phyloseq* R package](https://www.bioconductor.org/packages/release/bioc/html/phyloseq.html), including data manipulations, taxonomy handlers ([Silva](https://www.arb-silva.de/), [Greengenes2](https://greengenes2.ucsd.edu/), [PR2](https://pr2-database.org/), ...), phyloseq-shortcuts (data normalisation, phyloseq splitting and exporting) and graphic visualization.

This repository is currently in **ALPHA** state. Nothing is guaranteed and the material is subject to change without a notice (e.g., function names or arguments).  

# Installation

```r
# Installing devtools if it is not already installed
install.packages("devtools")

# Installing metaBmisc
suppressWarnings(suppressMessages(devtools::install_gitlab(repo = "cn7ab95/metabmisc", host = "gitlab.ifremer.fr")))

# Loading metaBmisc
suppressWarnings(suppressMessages(library(metaBmisc)))
```


# Package features

A total of 28 functions are currently available through the `metaBmisc` R package. Here is the set of available functions whose help can be displayed in R using the following command :

```r
?bubble_plot_glom
?create_phyloseq
...
```

- `beta_div_ordination()`: A R function to perform ordination in beta diversity analyses
- `bubble_plot_glom()`: A R function to easily create a Bubble plot from phyloseq object
- `create_phyloseq()`: A R function to easily create a phyloseq object from a metabarcoding count table with ASV taxonomy, a metadata table and a phylogenetic tree (optional)
- `decontam_batch()`: A R function to identify and remove contaminant in microbiome analyses
- `dist_within_between_groups()`: A R function to calculate and plot distances within and between sample groups
- `export_phyloseq_to_df()`: A R function to convert phyloseq object to a full data frame
- `extract_core_ASVs()`: A R function to extract core ASVs or Taxa between group of samples or all samples
- `extract_non_shared_ASVs()`: A R function to extract non-shared ASVs or Taxa between group of samples or all samples
- `extract_top_taxa()`: A R function to extract top most abundant ASVs/taxa
- `filter_phyloseq()`: A R function to easily filter in a single function a phyloseq object by sample name and/or by sample depth and/or by ASVs taxonomy and/or by ASVs contingency and/or by ASVs abundance and/or by ASV relative abundance
- `lda_effect_size()`: A R function to perform Metabarcoding LEFSe analysis based on phyloseq object
- `merge_samples_metadata()`: A R function to merge samples of a phyloseq object according to metadata variable while recreating the associated metadata
- `network_analysis()`: A R function to perform network analysis. This function generates a network (".gml" file) file that can be viewed and edited using [Cytoscape](https://cytoscape.org/)
- `normalize_by_coverage_rarefactions()` : A R function to performs coverage-based multiple rarefactions and return a consensus of all iterations
- `normalize_by_CSS_method()` : A R function to performs a Cumulative Sum Scaling (CSS normalization of microbiome data
- `normalize_by_DESeq2_method()` : A R function to performs microbiome data normalization using DESeq2
- `normalize_by_multiple_rarefactions()` : A R function to performs microbiome data normalization by multiple rarefactions
- `plot_align_asv_seqs()`: A R function to plot multiple ASV sequence alignment using ggplot2
- `plot_alpha_div()`: A R function to plot compute and plot alpha diversity indices from phyloseq object
- `plot_barplot_abund()`: A R function to plot relative abundance of the n most abundant taxa from a user-defined taxonomic rank while keeping the information at a higher taxonomic level
- `plot_diff_relabund()`: A R function to visualize the relative abundance differentials of the top n taxa in different conditions
- `plot_pca_tax_loadings()`: A R function to easily plot PCA analysis of microbial data with taxa “loadings” onto the PCA axes
- `plot_pie_abund()`: A R function to plot pie chart of the global relative abundance of the n most abundant taxa from a user-defined taxonomic rank
- `plot_raref_curves()`: A R function to plot rarefaction curve from phyloseq object
- `summarize_abund_prev()`: A R function to generates a summary of the abundance and prevalence of either ASVs or taxa of a desired taxonomic rank
- `tax_glom_replace()`: A R function to agglomerate ASVs/OTUs from the same taxa and replace row names of the count table by the taxonomic assignment
- `taxonomy_homogenization()`: A R function to homogenize the taxonomic ranks for the different ASVs of the same taxa. 
- `upsetr_phyloseq()`:  A R function to  visualize set intersections using UpSetR matrix design directly from phyloseq object


## Dependencies

All dependencies below are automatically downloaded, installed and loaded when you install `metaBmisc`

- *BiocGenerics* (>= 0.46.0)
- *classInt* (>= 0.4.10)
- *ComplexUpset* (>= 1.3.3)
- *decontam* (>= 1.20.0)
- *DESeq2* (>= 1.40.2)
- *dplyr* (>= 1.1.4)
- *ggh4x* (>= 0.2.8)
- *ggmsa* (>= 1.3.4)
- *ggnested* (>= 0.1.0)
- *ggplot2* (>= 3.5.0)
- *ggpubr* (>= 0.6.0)
- *ggsci* (>= 3.1.0)
- *gplots* (>= 3.1.3.1)
- *iNEXT* (>= 3.0.1)
- *lemon* (>= 0.4.9)
- *magrittr* (>= 2.0.3)
- *metagMisc* (>= 0.5.0)
- *microbiome* (>= 1.22.0)
- *microbiomeMarker* (>= 1.6.0)
- *microViz* (>= 0.12.3)
- *muscle* (>= 3.42.0)
- *NetCoMi* (>= 1.1.0)
- *netpredictor* ()
- *paletteer* (>= 1.6.0))
- *phyloseq* (>= 1.44.0)
- *randomcoloR* (>= 1.1.0.1)
- *RColorBrewer* (>= 1.1.3)
- *reshape2* (>= 1.4.4)
- *rstatix* (>= 0.7.2)
- *SpiecEasi* (>= 1.1.3)
- *stringr* (>= 1.5.1)
- *svMisc* (>= 1.2.3)
- *tibble* (>= 3.2.1)
- *tidyr* (>= 1.3.1)
- *vegan* (>= 2.6.4)

# Contributions

We welcome contributions to the pipeline. If such case you can do one of the following:

- Use issues to submit your questions
- Fork the project, do your developments and submit a pull request
- Contact me (see [email below](https://gitlab.ifremer.fr/cn7ab95/metabmisc/-/blob/main/README.md?ref_type=heads#support))

# Citation and Acknowledgements

To cite metaBmisc : 

Noël C. (2024). metaBmisc: miscellaneous functions for metabarcoding analysis. R package version 0.1

`metaBmisc` stands on the shoulders of numerous R-packages (see Dependencies). In particular, it would not have happened without [*metagMisc*](https://github.com/vmikk/metagMisc), [*phyloseq*](https://github.com/joey711/phyloseq/) and [*vegan*](https://github.com/vegandevs/vegan/) packages. Please cite R and R packages when you use them for data analysis.


# Support

For further information or help, don't hesitate to get in touch with me:

![my email](./assets/cnoel-email.png)