# metaBmisc v.0.1.0 (May 07, 2024)

- New functions
   * `create_phyloseq.R` - A R function to easily create a phyloseq object
   * `tax_glom_rownames.R` - A R function to agglomerate ASV/OTU from the same taxa with renaming row names in count table with taxonomy
   * `calculate_avg_phyloseq_cov_raref.R` - A R function to perform Coverage-based multiple rarefactions and build a consensus phyloseq object
   * `bubble_plot_glom.R` - A R function to easily create a Bubble plot from phyloseq object