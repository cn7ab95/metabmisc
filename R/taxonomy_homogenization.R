#' Reformat taxonomy from phyloseq
#'
#' @description
#' This function makes it possible to homogenize the taxonomic ranks for
#' the different ASVs of the same taxa. Indeed, some taxonomic
#' databases for the same genus sometimes have different family names.
#'
#' @param physeq (Required). A phyloseq object.
#'
#' @import phyloseq
#' @export
#'
#' @examples
#' # Import metaBmisc example data
#' data(birds)
#' physeq <- birds$phyloseq
#'
#' # Taxonomy homogenization
#' reformatted_physeq <- taxonomy_homogenization(physeq)
#'
taxonomy_homogenization <- function (physeq) {

  # Function to homogenize the taxonomy of previous taxonomic ranks for identical taxa
  homogenize <- function(df) {
    for (i in seq_along(df$Genus)) {
      genus_value <- df$Genus[i]
      if (i > 1 && df$Genus[i] == df$Genus[i - 1]) {
        df[i, 1:(which(names(df) == "Genus") - 1)] <- df[i - 1, 1:(which(names(df) == "Genus") - 1)]
      } else {
        matching_rows <- df$Genus == genus_value
        if (sum(matching_rows) > 1) {
          df[matching_rows, 1:(which(names(df) == "Genus") - 1)] <- df[i, 1:(which(names(df) == "Genus") - 1)]
        }
      }
    }
    return(df)
  }

  # Raw taxonomy table
  taxonomy_table <- data.frame(tax_table(physeq), check.names = FALSE)

  # Apply the homogenization function
  reformatted_taxonomy <- homogenize(taxonomy_table)

  # Import reformatted taxonomy
  if (!is.null(physeq@phy_tree) & !is.null(physeq@refseq)) {
    physeq <- suppressMessages(suppressWarnings(phyloseq(otu_table(physeq), tax_table(as.matrix(reformatted_taxonomy)), sample_data(physeq), phy_tree(physeq), refseq(physeq))))
  } else if (!is.null(physeq@phy_tree) & is.null(physeq@refseq)) {
    physeq <- suppressMessages(suppressWarnings(phyloseq(otu_table(physeq), tax_table(as.matrix(reformatted_taxonomy)), sample_data(physeq), phy_tree(physeq))))
  } else if (is.null(physeq@phy_tree) & !is.null(physeq@refseq)) {
    physeq <- suppressMessages(suppressWarnings(phyloseq(otu_table(physeq), tax_table(as.matrix(reformatted_taxonomy)), sample_data(physeq), refseq(physeq))))
  } else {
    physeq <- suppressMessages(suppressWarnings(phyloseq(otu_table(physeq), tax_table(as.matrix(reformatted_taxonomy)), sample_data(physeq))))
  }

  return(physeq)

}
