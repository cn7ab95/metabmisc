#' Plot the abundance of differential taxa
#'
#' @description
#' This R function allows you to visualize the relative abundance differentials between different taxa in different conditions
#'
#' @param physeq (Required). A phyloseq object.
#' @param sample_group (Required). A character string specifying the sample grouping variable. Should be among the results of sample_variables(physeq).
#' @param tax_rank (Optional). A character to specify taxonomic rank to perform differential analysis on. Should be among rank_names(physeq). Default = NULL which means differential analysis is performed on taxa_names(phyloseq), e.g., OTU or ASV).
#' @param ntaxa Number of top abundance differentials to show (Default = 10).
#' @param legend_color_key_order (Optional). Vector used to order values of the legend_color_key; i.e. c("S1", "S3", "S2").
#' @param color_values (Optional). Vector containing the desired barplot colors. By default, a color palette of 10 colors is used.
#'
#' @import phyloseq
#' @import reshape2
#' @import ggplot2
#' @import ggpubr
#' @import dplyr
#' @import stringr
#' @export
#'
#' @examples
#' # Import metaBmisc example data
#' data(birds)
#' physeq <- birds$phyloseq
#'
#' # Visualize the top 5 abundance differentials between Genus according to bird_family
#' plot_diff_relabund(physeq, sample_group = "bird_family", tax_rank = "Genus", ntaxa = 5)
#'
plot_diff_relabund <- function (physeq, sample_group = NULL, tax_rank = NULL, ntaxa = 10, legend_color_key_order = NULL, color_values = NULL) {

  if (taxa_are_rows(physeq)) {
    physeq_glom <- tax_glom_rownames(physeq, taxrank = tax_rank, taxa_are_row = TRUE)
  } else {
    physeq_glom <- tax_glom_rownames(physeq, taxrank = tax_rank, taxa_are_row = FALSE)
  }
  physeq_glom_relabund <- transform_sample_counts(physeq_glom, function(x) x / sum(x) * 100)

  asv_table <- data.frame(otu_table(physeq_glom_relabund), check.rows = FALSE, check.names = FALSE)

  # Calculation of the total variance of each taxon #
  asv_table$total_variance <- apply(asv_table, 1, var)
  asv_table <- asv_table[order(asv_table$total_variance, decreasing = TRUE), ]

  # Top abundance differentials #
  top_abundance_differentials <- asv_table[1:ntaxa, -length(asv_table)]
  top_abundance_differentials[[tax_rank]] <- rownames(top_abundance_differentials)
  invisible(suppressMessages(suppressWarnings(melt_top_abundance_differentials <- melt(top_abundance_differentials))))
  colnames(melt_top_abundance_differentials) <- c(tax_rank, "sampleID", "abundance")

  metadata <- data.frame(sample_data(physeq), check.rows = FALSE, check.names = FALSE)

  melt_top_abundance_differentials_sdf <- merge(melt_top_abundance_differentials, metadata, by.x = "sampleID", by.y = "row.names", all.x = TRUE)

  # Mean abundance and standard error #
  invisible(suppressMessages(suppressWarnings(data_summary <- melt_top_abundance_differentials_sdf %>% group_by(.data[[tax_rank]], .data[[sample_group]]) %>% summarise(mean_abundance = mean(abundance), sd_abundance = sd(abundance)))))
  invisible(suppressMessages(suppressWarnings(obs_count <- melt_top_abundance_differentials_sdf %>% group_by(.data[[tax_rank]], .data[[sample_group]]) %>% count())))
  data_summary$n <- obs_count$n

  # Plot #
  data_summary$Genus <- str_replace_all(data_summary$Genus, "_", " ")
  data_summary$Genus <- str_replace_all(data_summary$Genus, "Unknown ", "Unknown\n")
  # specific case of Clostridium sensu stricto
  data_summary$Genus <- str_replace_all(data_summary$Genus, "Clostridium sensu stricto", "Clostridium\nsensu stricto")

  if (!is.null(legend_color_key_order)) {
    data_summary[[sample_group]] <- factor(data_summary[[sample_group]], levels = legend_color_key_order)
  }

  if (!is.null(color_values)) {
    vec_colors <- color_values
  } else {
    vec_colors <- distinct_palette(n = 10, pal = "kelly")
  }

  ggplot(data_summary, aes(x = .data[[tax_rank]], y = mean_abundance, fill = .data[[sample_group]], color = .data[[sample_group]])) +
    geom_bar(stat = "identity", position = position_dodge(width = 0.6), width = 0.5) +
    geom_errorbar(aes(ymin = mean_abundance - (sd_abundance / sqrt(n)), ymax = mean_abundance + (sd_abundance / sqrt(n))), position = position_dodge(width = 0.6), width = 0.3, color = "black") +
    theme_minimal() +
    theme(panel.grid.major.x = element_blank()) +
    theme(panel.grid.major.y = element_line(color = "black", linetype = 2, linewidth = 0.3)) +
    theme(panel.grid.minor.y = element_line(color = "grey85", linetype = 2, linewidth = 0.3)) +
    theme(axis.text = element_text(color = "black", size = 12)) +
    theme(axis.text.x = element_text(hjust = 0.5, vjust = 0.5, margin = margin(t = -0.5, unit = "cm"))) +
    theme(axis.title.x = element_blank()) +
    theme(axis.title.y = element_text(color = "black", size = 14, face = "bold", margin = margin(r = 0.5, unit = "cm"))) +
    labs(fill = "", color = "", y = "Mean relative abundance (%)") +
    theme(legend.text = element_text(size = 12, color = "black")) +
    scale_color_manual(values = vec_colors) +
    scale_fill_manual(values = vec_colors)

}

