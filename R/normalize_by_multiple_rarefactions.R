#' Microbiome data normalization by multiple rarefactions
#'
#' @description
#' This function performs multiple rarefaction iterations of a microbiome data
#' phyloseq object and renders a normalized phyloseq object from the consensus
#' of the rarefaction iterations. This function is based on the
#' \code{\link[metagMisc]{phyloseq_mult_raref_avg}}
#' function of the `metagMisc` package
#'
#' @param physeq (Required). A phyloseq object.
#' @param iter Number of rarefication iterations (default = 100).
#'
#' @import phyloseq
#' @import metagMisc
#' @export
#'
#' @examples
#' # Import metaBmisc example data
#' data(birds)
#' physeq <- birds$phyloseq
#'
#' # Normalize data by multiple rarefactions
#' physeq_rarefied <- normalize_by_multiple_rarefactions(physeq, iter = 10)
#' physeq_rarefied
normalize_by_multiple_rarefactions <- function(physeq, iter = 100 , parallel = FALSE) {

  min_sample_size <- min(sample_sums(physeq))
  invisible(capture.output(suppressMessages(physeq_mult_raref <- metagMisc::phyloseq_mult_raref_avg(physeq, SampSize = min_sample_size, iter = iter, parallel = parallel))))

  return(physeq_mult_raref)

}
