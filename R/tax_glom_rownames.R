#' Agglomerate ASV/OTU from the same taxa
#'
#' @description
#' This method merges ASVs/OTUs that have the same taxonomy at a certain taxonomic rank.
#' Its approach is analogous to tax_glom, this function also automatically replaces the name of the ASVs with its taxonomic assignment.
#'
#' /!\ Duplicate row.name error /!\
#'
#' If you encounter this error, it is because in your taxonomic database for same taxa there are several
#' different taxonomy (e.g. for a genus you have two different families one with the old taxonomic nomenclature and one with the recent): this is the case for the SILVA database.
#'
#' --> To resolve this problem, you can use the `metaBmisc::taxonomy_homogenization()` function
#'
#' @param physeq (Required). A phyloseq object.
#' @param taxrank (Required). A character string specifying the taxonomic level that you want to agglomerate over. Should be among the results of rank_names(physeq).
#' @param taxa_are_row Logical indicating the orientation of the abundance table contained in object x (default = TRUE).
#'
#' @import phyloseq
#' @export
#'
#' @examples
#' # Import metaBmisc example data
#' data(birds)
#' physeq <- birds$phyloseq
#' physeq
#'
#' # Agglomerate ASV at the Genus level
#' physeq_genus <- tax_glom_rownames(physeq, taxrank = "Genus")
#' physeq_genus
tax_glom_rownames <- function (physeq, taxrank, taxa_are_row = TRUE) {

  # Agglomerate ASV (count sum) by tax rank
  physeq_glom <- suppressMessages(tax_glom(physeq, taxrank))

  # Retrieve new ASV table
  if (taxa_are_row) {
    table_glom <- data.frame(otu_table(physeq_glom), check.rows = FALSE, check.names = FALSE)
  } else {
    table_glom <- data.frame(t(otu_table(physeq_glom)), check.rows = FALSE, check.names = FALSE)
  }

  # Retrieve tax table
  tax <- data.frame(tax_table(physeq_glom), check.rows = FALSE, check.names = FALSE)

  # Change row names of ASV table and tax
  rownames(table_glom) <- tax[, taxrank]
  rownames(tax) <- tax[, taxrank]

  # Build the new phyloseq object with tax rank ID in rownames
  physeq_glom_replace <- phyloseq(otu_table(table_glom, taxa_are_rows = TRUE), tax_table(as.matrix(tax)), sample_data(physeq_glom))

  return(physeq_glom_replace)

}
